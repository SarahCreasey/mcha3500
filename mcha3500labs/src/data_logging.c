/* -----------------------------------------------------
Data Logging C file 

Author: Sarah Creasey
Date Created: 29/08/2021
Last Modified: 13/09/2021

Description: 
Reads potentiometer voltage and prints to the serial port
Modified osTimer from pendulum specific to generic data logger
--------------------------------------------------------
*/

#include "data_logging.h"

#include <stdint.h>					// Includes standard integer types
#include <stdlib.h>					// Defines functions converting between strings and number
#include <math.h>					// Defines math functions (trigonemtric)

#include "stm32f4xx_hal.h"			// Includes all HAL functions
#include "cmsis_os2.h"				// Includes all RTOS functions
#include "uart.h"					// Allows print to console via printf
#include "pendulum.h"
#include "IMU.h"


// Variable declarations
uint16_t logCount;
static osTimerId_t _pendloggingTimerID;
static osTimerAttr_t _pendloggingTimerAttr = {
    .name = "pendDataLogging" 
};
static uint8_t _is_running = 0;
static uint8_t _is_init = 0;
static void (*log_function)(void);              // log_function points to a function (function pointer variable)


// Function declarations
static void log_pendulum(void);
void logging_init(void);
void pend_logging_start(void);
void logging_stop(void);
static void log_pointer(void *argument);


// Function definitions
static void log_pendulum(void)
{
    // Supress compiler warnings for unused arguments
    //UNUSED(argument);

    // Read the potentiometer voltage
    float voltage = pendulum_read_voltage();

    // Print the sample time and potentiometer voltage to the serial terminal 
    printf("%f,      %f\n", logCount*0.005, voltage);

    // Increment log count
    logCount++;

    // Stop logging once 2 seconds is reached, 2 seconds at 200Hz = 400 logCounts
    if(logCount >= 400)
    {
        logging_stop();
    }

}

void logging_init(void)
{
    //  Create an osTimer using osTimerNew().
    //    -   Store the id in _pendulumTimerID
    //    -   Timer's callback function is log_pendulum
    //    -   Configure it to be periodic
    //    -   No input argument therefore argument is NULL
    //    -   Configure the attributes by giving `attr` a pointer to _heartbeatTimerAttr */
    
    // osTimer modified for use with generic logging function
   _pendloggingTimerID = osTimerNew(log_pointer, osTimerPeriodic, NULL, &_pendloggingTimerAttr);

   //pend_logging_start();
    //imu_logging_start();

    _is_init = 1;
}

static void log_pointer(void *argument)
{
    UNUSED(argument);

    // Call function pointed to by log_function
    (*log_function)();
}

void pend_logging_start(void)
{   
    // Change function pointer to the pendulum logging function
    log_function = &log_pendulum;

    // Reset the log counter
    logCount = 0;

    if (!_is_running)
    {
        // Start data logging osTimer at 200Hz (5ms)
        osTimerStart(_pendloggingTimerID, 5);
        _is_running = 1;
    }
}

void logging_stop(void)
{
    if (_is_running)
    {
        // Stop the data logging osTimer
        osTimerStop(_pendloggingTimerID);
        _is_running = 0;
    }
}

void imu_logging_start(void)
{
    // Change function pointer to the imu logging function (log_imu)
    log_function = &log_imu;

    // Reset the log counter
    logCount = 0;

    // Start the data logging at 200Hz
    if (!_is_running)
    {
        // Start data logging osTimer at 200Hz (5ms)
        osTimerStart(_pendloggingTimerID, 5);
        _is_running = 1;
    }
}

static void log_imu(void)
{
    // Read IMU
    IMU_read();

    // Get the IMU angle from the Accelerometer readings
    double theta = get_acc_angle();

    // Get the IMU Gyroscope X reading
    float W = get_gyroX();

    // Read the potentiometer voltage
    float pendVol = pendulum_read_voltage();

    // Print the time, accelerometer angle, gyro angular velocity and poteniometer voltage to the serial terminal
    printf("%f, %f, %f, %f\n", logCount*0.005, theta, W, pendVol);

    // Increment log count
    logCount++;

    // Stop logging once 5 seconds has been reached, 5 seconds at 200Hz = 1000 logCounts
    if(logCount >= 1000)
    {
        logging_stop();
    }
}


