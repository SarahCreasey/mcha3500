/* -----------------------------------------------------
Pendulum C file 

Author: Sarah Creasey
Date Created: 16/08/2021
Last Modified: 16/08/2021

Description: 
Initialisation of peripherals for pendulum
Reads and returns voltage on PB0
--------------------------------------------------------
*/

#include <stdint.h>					// Includes standard integer types
#include <stdlib.h>					// Defines functions converting between strings and number
#include <math.h>					// Defines math functions (trigonemtric)

#include "stm32f4xx_hal.h"			// Includes all HAL functions
#include "cmsis_os2.h"				// Includes all RTOS functions
#include "uart.h"					// Allows print to console via printf
#include "pendulum.h"				// Allows the call of other functions in pendulum.c


static GPIO_InitTypeDef  GPIO_InitStructure;
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;


void pendulum_init(void)
{
	// Enable ADC1 clock and GPIOB clock
	__HAL_RCC_ADC1_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	// Initialise PB0 with:
	// 			- Pin 0
	// 			- Analog mode
	// 			- No pull
	// 			- High frequency
    GPIO_InitStructure.Pin = GPIO_PIN_0;
    GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    // Initialise ADC1 with:
    //			- Instance ADC 1
    //			- Div 2 prescaler
    //			- 12 bit resolution
    // 			- Data align right
    //			- Continuous conversion mode disabled
    // 			- Number of conversions = 1
    hadc1.Instance = ADC1;
    hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc1.Init.Resolution = ADC_RESOLUTION_12B;
    hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc1.Init.ContinuousConvMode = DISABLE;
    hadc1.Init.NbrOfConversion = 1;
    HAL_ADC_Init(&hadc1);

    // Configure ADC channel to:
    //			- Channel 8
    // 			- Rank 1
    // 			- Sampling time 480 cycles
    // 			- Offset 0
    sConfigADC.Channel = ADC_CHANNEL_8;
    sConfigADC.Rank = 1;
    sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
    sConfigADC.Offset = 0;
    HAL_ADC_ConfigChannel(&hadc1, &sConfigADC);
}


// Reads the voltage of PB0 and returns the voltage as a float
float pendulum_read_voltage(void)
{
	// Start ADC
	HAL_ADC_Start(&hadc1);

	// Poll for conversion using a timeout of OxFF
	HAL_ADC_PollForConversion(&hadc1, 0xFF);

	// Get ADC value
	float ADC_voltage = HAL_ADC_GetValue(&hadc1);

	// Stop ADC
	HAL_ADC_Stop(&hadc1);

	// Compute voltage from ADC reading
	float voltage = ADC_voltage*(3.3/4095.0);

	// Return the computed voltage
	return voltage;
}

