#ifndef DATA_LOGGING_H
#define DATA_LOGGING_H

/* Add function prototypes here */
static void log_pendulum(void);
void logging_init(void);
void pend_logging_start(void);
void logging_stop(void);
static void log_pointer(void *argument);
void imu_logging_start(void);
static void log_imu(void);

#endif
