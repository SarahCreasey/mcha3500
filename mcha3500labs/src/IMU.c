/* -----------------------------------------------------
IMU C file 

Author: Sarah Creasey
Date Created: 13/09/2021
Last Modified: 13/09/2021

Description: 
Initialises IMU peripheral and retrives/converts sensor values
--------------------------------------------------------
*/

#include "IMU.h"					

#include <stdint.h>					// Includes standard integer types
#include <stdlib.h>					// Defines functions converting between strings and number
#include <math.h>					// Defines math functions (trigonemtric)

#include "stm32f4xx_hal.h"			// Includes all HAL functions
#include "cmsis_os2.h"				// Includes all RTOS functions
#include "uart.h"					// Allows print to console via printf

#include "tm_stm32_mpu6050.h"


// Variable declarations
TM_MPU6050_t IMU_datastruct;

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


// Function definitions
void IMU_init(void)
{
    // Initialise MPU6050 and I2C peripheral with a +- 4G range (accelerometer) and a +- 250 degree/s range (gyroscope)
    TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
}

void IMU_read(void)
{
    // Read all IMU values
    TM_MPU6050_ReadAll(&IMU_datastruct);
}

float get_accY(void)
{
    // Retrieve value of Y acceleration from IMU
    // note: IMU_datastruct stores Accelerometer value for Y axis as int16_t, want to convert to float
    //       +- 4g is over the range -32,786 to 32,767 (65553/8 = 8194.125)
    float Ay = IMU_datastruct.Accelerometer_Y/8194.125;

    // Convert acceleration reading to units m/s^2
    Ay = Ay*9.81;

    // Return the Y acceleration
    return Ay;
}

float get_accZ(void)
{
    // Retrieve value of Z acceleration from IMU
    float Az = IMU_datastruct.Accelerometer_Z/8194.125;

    // Convert acceleration reading to units m/s^2
    Az = Az*9.81;

    // Return the Y acceleration
    return Az;
}

float get_gyroX(void)
{
    // Retrieve the gyroscope reading in X co-ordinate
    float Vx = IMU_datastruct.Gyroscope_X/131.106;

    // Convert angular velocity to radians per second
    Vx = Vx*M_PI/180.0;

    // Return the X angular velcoity
    return Vx;
}

double get_acc_angle(void)
{
    // Compute IMU angle using accY and accZ via atan2
    double theta = -atan2(get_accZ(), get_accY());

    // Return the IMU angle
    return theta;
}

