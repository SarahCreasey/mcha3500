/* -----------------------------------------------------
Motor C file 

Author: Sarah Creasey
Date Created: 16/08/2021
Last Modified: 22/08/2021

Description: 
Initialisation of peripherals for motor
--------------------------------------------------------
*/

#include "motor.h"					// Allows the call of other functions in motor.c

#include <stdint.h>					// Includes standard integer types
#include <stdlib.h>					// Defines functions converting between strings and number
#include <math.h>					// Defines math functions (trigonemtric)

#include "stm32f4xx_hal.h"			// Includes all HAL functions
#include "cmsis_os2.h"				// Includes all RTOS functions
#include "uart.h"					// Allows print to console via printf
#include "pendulum.h"				// Allows the call of other functions in pendulum.c


#define TIMERPERIOD 10000
static GPIO_InitTypeDef  GPIO_InitStructure;
static TIM_HandleTypeDef   _htim3;
static TIM_OC_InitTypeDef  _sConfigPWM;
int enc_count;


void motor_PWM_init(void)
{
	// Enable TIM3 clock and GPIOA clock
	__TIM3_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();

	// Initialise PA6 with:
	// 			- Pin 6
	// 			- Alternate function push pull mode
	// 			- No pull
	// 			- High frequency
	//			- Alternate function 2: Timer 3
	GPIO_InitStructure.Pin = GPIO_PIN_6;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStructure.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

    // Initialise timer 3 with:
    //			- Instance TIM3
    //			- Prescaler of 1
    //			- Counter mode up
    // 			- Timer period to generate a 10kHz signal
    // 			- Clock division of 0
    _htim3.Instance = TIM3;
    _htim3.Init.Prescaler = 1;
    _htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    _htim3.Init.Period = TIMERPERIOD;
    _htim3.Init.ClockDivision = 0;
    HAL_TIM_PWM_Init(&_htim3);

    // Configure timer 3, channel 1 with:
    //			- Output compare mode PWM1
    // 			- Pulse = 0
    //			- OC polarity high
    //			- Fast mode disabled
    _sConfigPWM.OCMode = TIM_OCMODE_PWM1;
    _sConfigPWM.Pulse = 0;
    _sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH;
    _sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_PWM_ConfigChannel(&_htim3, &_sConfigPWM, TIM_CHANNEL_1);

    // Set initial timer 3, channel 1 compare value
    int brightness = TIMERPERIOD*0.25;
    __HAL_TIM_SET_COMPARE(&_htim3, TIM_CHANNEL_1, brightness);

    // Start timer 3, channel 1
    HAL_TIM_PWM_Start(&_htim3, TIM_CHANNEL_1);
}


void motor_encoder_init(void)
{
	// Enable GPIOC clock
	__HAL_RCC_GPIOC_CLK_ENABLE();

	// Initialise PC0 and PC1 with:
	// 			- Pin 0|1
	// 			- Interupt rising and falling edge
	// 			- No pull
	// 			- High frequency
	GPIO_InitStructure.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING ;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

    // Set priority of external interupt lines 0, 1 to 0x0f, 0x0f
    // Enable external interupt for lines 0, 1
    HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);

    HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);
}


void EXTI0_IRQHandler(void)
{
	// Check if PC0 == PC1 and adjust encoder count accordingly
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
	{
		enc_count++;
	}
    else
    {
        enc_count--;
    }

	// Reset interupt
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

void EXTI1_IRQHandler(void)
{
    // Check if PC0 == PC1 and adjust encoder count accordingly
    if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
    {
        enc_count--;
    }
    else
    {
        enc_count++;
    }

    // Reset interupt
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}


int motor_encoder_getValue(void)
{
	return enc_count;
}
